package com.shyndard.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.shyndard.entity.Information;

@Path("/api/v1/scaleway")
public class ScalewayResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Information hello() {
        return new Information("Hello scaleway", "How are you ?");
    }
}