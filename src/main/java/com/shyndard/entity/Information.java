package com.shyndard.entity;

public class Information {
    
    private String title;
    private String message;

    public Information(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }
}
